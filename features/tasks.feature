Feature: Manage Tasks
  In order to accomplish my work
  As an User
  I want to create and manage tasks

  Scenario: Create Task
	Given I go to the new tasks form
	When I fill in that form and submit
	Then I should see that task