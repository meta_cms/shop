Given /^I go to the new tasks form$/ do
  visit tasks_path
  click_link "New Task"
end

Given /^I fill in that form and submit$/ do
  fill_in 'task[name]', :with => "Testtask"
end

Then /^I should see that task$/ do
  click_button 'Create Task'
  page.should have_content("Testtask")
end
